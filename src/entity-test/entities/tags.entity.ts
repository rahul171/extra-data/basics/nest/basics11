import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Table14 } from './table14.entity';

@Entity()
export class Tag {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column()
  name: string;

  @Column()
  description?: string;

  @ManyToOne(
    type => Table14,
    table14 => table14.tags,
  )
  @JoinColumn()
  table14?: Table14;
}
