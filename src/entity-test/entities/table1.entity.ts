import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Table3 } from './table3.entity';

@Entity()
export class Table1 {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @OneToOne(() => Table3, {
    cascade: true,
  })
  @JoinColumn()
  table3: Table3;
}
