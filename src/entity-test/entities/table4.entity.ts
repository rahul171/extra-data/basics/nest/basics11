import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Table4 {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name1: string;

  // if not nullable (which is the default behaviour), then the value will
  // be empty => ''
  // if nullable, then value => NULL
  @Column({
    nullable: true,
  })
  name2: string;
}
