import {
  Column,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Table13 } from './table13.entity';

@Entity()
export class Table12 {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column()
  name: string;

  @Column({
    default: 0,
  })
  num?: number;

  @OneToOne(() => Table13, {
    eager: true,
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  table13?: Table13;

  @DeleteDateColumn()
  deletedAt?: Date;
}
