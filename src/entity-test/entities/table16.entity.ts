import {
  Column,
  Entity,
  JoinColumn,
  ManyToMany,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Table15 } from './table15.entity';

@Entity()
export class Table16 {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @OneToOne(
    () => Table15,
    table15 => table15.table161,
  )
  table151: Table15;

  @ManyToOne(
    () => Table15,
    table15 => table15.table162,
  )
  @JoinColumn()
  table152: Table15;

  @ManyToMany(
    () => Table15,
    table15 => table15.table163,
  )
  table153: Table15;
}
