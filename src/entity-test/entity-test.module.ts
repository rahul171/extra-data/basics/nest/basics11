import { Module } from '@nestjs/common';
import { EntityTestService } from './services/entity-test.service';
import { EntityTestController } from './entity-test.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Table1 } from './entities/table1.entity';
import { Table2 } from './entities/table2.entity';
import { Table3 } from './entities/table3.entity';
import { Table4 } from './entities/table4.entity';
import { Table5 } from './entities/table5.entity';
import { Table6 } from './entities/table6.entity';
import { Table7 } from './entities/table7.entity';
import { Table8 } from './entities/table8.entity';
import { Table9 } from './entities/table9.entity';
import { Table10 } from './entities/table10.entity';
import { Table11 } from './entities/table11.entity';
import { Table12 } from './entities/table12.entity';
import { EntityTest2Service } from './services/entity-test2.service';
import { Table13 } from './entities/table13.entity';
import { EntityTest3Service } from './services/entity-test3.service';
import { Table14 } from './entities/table14.entity';
import { Tag } from './entities/tags.entity';
import { Table15 } from './entities/table15.entity';
import { Table16 } from './entities/table16.entity';
import { EntityTest4Service } from './services/entity-test4.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Table1,
      Table2,
      Table3,
      Table4,
      Table5,
      Table6,
      Table7,
      Table8,
      Table9,
      Table10,
      Table11,
      Table12,
      Table13,
      Table14,
      Tag,
      Table15,
      Table16,
    ]),
  ],
  providers: [
    EntityTestService,
    EntityTest2Service,
    EntityTest3Service,
    EntityTest4Service,
  ],
  controllers: [EntityTestController],
})
export class EntityTestModule {}
