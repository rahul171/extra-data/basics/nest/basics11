import {
  Entity,
  Tree,
  Column,
  PrimaryGeneratedColumn,
  TreeChildren,
  TreeParent,
  TreeLevelColumn,
} from 'typeorm';

@Entity()
@Tree('closure-table')
export class Table9 {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @TreeChildren()
  children: Table9[];

  @TreeParent()
  parent: Table9;

  @TreeLevelColumn()
  level: number;
}
