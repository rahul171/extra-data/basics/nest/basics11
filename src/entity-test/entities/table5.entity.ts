import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity()
export class Table5 {
  @PrimaryColumn()
  name: string;

  @Column()
  message: string;
}
