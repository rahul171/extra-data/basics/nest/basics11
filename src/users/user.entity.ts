import {
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { SubUser } from './sub-user.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  message: string;

  @Column({ default: true })
  active: boolean;

  @ManyToMany(() => SubUser)
  // @JoinColumn({ referencedColumnName: 'id' })
  // @JoinColumn()
  @JoinTable()
  subUser: SubUser;

  @ManyToOne(() => SubUser)
  @JoinColumn([
    { referencedColumnName: 'id' },
    { referencedColumnName: 'childName' },
  ])
  subUser1: SubUser;

  // @OneToOne(() => SubUser)
  // @JoinColumn({
  //   name: 'subName',
  //   referencedColumnName: 'childName',
  // })
  // subUser1: SubUser;
}
