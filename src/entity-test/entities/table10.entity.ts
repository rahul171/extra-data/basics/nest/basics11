import {
  Column,
  Entity,
  Generated,
  PrimaryColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Table10 {
  @PrimaryColumn()
  id: number;

  // @Column()
  // @Generated('uuid')
  // id_uuid: string;

  // @Column()
  // @Generated('increment')
  // id_increment: number;

  // @Column()
  // @Generated('rowid')
  // id_rowid: number;

  @Column()
  name: string;
}
