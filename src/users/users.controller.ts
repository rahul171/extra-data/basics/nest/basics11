import { Body, Controller, Get, Param, Post, Query } from '@nestjs/common';
import { UsersService } from './users.service';
import { User } from './user.entity';
import { SaveUserDto } from './dtos/save-user.dto';
import { ValidationPipe } from '../common/pipes/validation.pipe';

@Controller('users')
export class UsersController {
  constructor(private usersService: UsersService) {}

  @Get('user')
  async getUsers(@Query('relation') relation): Promise<User[]> {
    if (relation === '1') {
      return this.usersService.getUsersWithRelation();
    }
    return this.usersService.getUsers();
  }

  @Get('subUser')
  async getSubUsers(@Query('relation') relation) {
    if (relation === '1') {
      return this.usersService.getSubUsersWithRelation();
    }
    return this.usersService.getSubUsers();
  }

  @Get('run-this')
  async runThis() {
    return this.usersService.runThis();
  }

  @Get('reset-db')
  async resetDB() {
    return this.usersService.resetDB();
  }

  @Post()
  async saveUser(
    @Body('user', ValidationPipe) user: SaveUserDto,
  ): Promise<User> {
    return this.usersService.saveUser(user);
  }

  // @Get('save/:id?')
  // save(@Param('id') id) {
  //   id = parseInt(id);
  //   if (!id) {
  //     id = 1;
  //   }
  //   return this.usersService.save(id);
  // }
}
