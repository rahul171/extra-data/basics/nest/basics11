import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Table13 {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;
}
