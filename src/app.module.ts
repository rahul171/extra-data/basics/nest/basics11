import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { AppService } from './app.service';
import { AppController } from './app.controller';
import { UsersModule } from './users/users.module';
import { DatabaseModule } from './database/database.module';

import cors = require('cors');
import { EntityTestModule } from './entity-test/entity-test.module';

@Module({
  imports: [
    // UsersModule,
    DatabaseModule,
    EntityTestModule,
  ],
  providers: [AppService],
  controllers: [AppController],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer): any {
    consumer.apply(cors()).forRoutes('/');
  }
}
