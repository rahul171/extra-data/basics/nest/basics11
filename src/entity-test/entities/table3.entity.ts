import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  VersionColumn,
} from 'typeorm';

@Entity()
export class Table3 {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name3: string;

  @CreateDateColumn()
  createdAt: Date;

  // if data is already available before adding this column, then the value
  // for this column for those rows will be the current date.
  @UpdateDateColumn()
  updatedAt: Date;

  // increased whenever a row updates.
  @VersionColumn()
  version: number;
}
