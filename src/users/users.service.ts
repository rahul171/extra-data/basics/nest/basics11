import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';
import { SubUser } from './sub-user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
    @InjectRepository(SubUser)
    private subUserRepository: Repository<SubUser>,
  ) {
    // this.internalRun();
  }

  async getUsers(): Promise<User[]> {
    return this.userRepository.find();
  }

  async getUsersWithRelation(): Promise<User[]> {
    return this.userRepository.find({ relations: ['subUser'] });
  }

  async getSubUsers() {
    return this.subUserRepository.find();
  }

  async getSubUsersWithRelation() {
    return this.subUserRepository.find({ relations: ['user'] });
  }

  async saveUser(user: any): Promise<User> {
    const newUser = new User();
    const keys = Object.keys(user);
    keys.forEach(key => {
      newUser[key] = user[key];
    });
    return this.userRepository.save(newUser);
  }

  async runThis() {
    const users = await this.userRepository.find({ relations: ['subUser'] });
    const subUsers = await this.subUserRepository.find({ relations: ['user'] });

    return { users, subUsers };
  }

  async resetDB() {
    return {
      user: await this.userRepository.query('drop table user;'),
      subUser: await this.userRepository.query('drop table sub_user;'),
    };
  }

  async save(i = 1) {
    const subUser1 = new SubUser();
    subUser1.childName = `childName${i}`;
    subUser1.childMessage = `childMessage${i}`;

    const subUser = await this.subUserRepository.save(subUser1);
    // console.log(subUser);

    // const response1 = await this.subUserRepository.findOne({ id: 1 });
    // console.log(response1);

    const user1 = new User();
    user1.name = `name${i}`;
    user1.message = `message${i}`;
    user1.subUser = subUser1;
    // user1.subUser1 = subUser1;
    // user1.subUser = response1;

    const user = await this.userRepository.save(user1);

    return { subUser, user };
  }

  async saveWithOld(i) {
    const subUser = await this.subUserRepository.findOne({ id: i });

    const user1 = new User();
    user1.name = `name${i}`;
    user1.message = `message${i}`;
    user1.subUser = subUser;

    const user = await this.userRepository.save(user1);

    return { subUser, user };
  }

  async internalRun() {
    await this.save(1);
    await this.save(2);
    await this.save(3);
    await this.saveWithOld(2);
    await this.saveWithOld(3);
  }
}
