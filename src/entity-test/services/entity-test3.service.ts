import { Injectable } from '@nestjs/common';
import {
  InjectConnection,
  InjectEntityManager,
  InjectRepository,
} from '@nestjs/typeorm';
import { Table14 } from '../entities/table14.entity';
import { Brackets, Connection, EntityManager, Repository } from 'typeorm';
import { Tag } from '../entities/tags.entity';

@Injectable()
export class EntityTest3Service {
  constructor(
    @InjectRepository(Table14)
    private table14Repository: Repository<Table14>,
    @InjectEntityManager()
    private entityManager: EntityManager,
    @InjectConnection()
    private connection: Connection,
  ) {
    this.runThis();
  }

  async runThis() {
    // const response = await this.table14Repository.query(
    //   "insert into table14 (name) values ('name1'), ('name2')",
    // );
    // console.log(response);
    //
    // const response = await this.table14Repository.clear();
    // console.log(response);
    //
    // const response = await this.table14Repository
    //   .createQueryBuilder('abcd')
    //   .insert()
    //   .values(
    //     (() => {
    //       const arr = [];
    //       for (let i = 0; i < 5; i++) {
    //         const tags = (n => {
    //           const tags = [];
    //           for (let j = 0; j < n; j++) {
    //             tags.push(
    //               this.entityManager.getRepository(Tag).create({
    //                 name: `insert-tag${j + 1}`,
    //               }),
    //             );
    //           }
    //           return tags;
    //         })(1 + Math.floor(Math.random() * 4));
    //         arr.push({
    //           name: `insert-name${i + 1}`,
    //           tags,
    //         });
    //       }
    //       return arr;
    //     })(),
    //   )
    //   // .getQuery();
    //   .execute();
    // console.log(response);
    //
    // const response1 = await this.entityManager
    //   .getRepository(Tag)
    //   .createQueryBuilder()
    //   .delete()
    //   .where(`name LIKE '%insert%'`)
    //   .execute();
    // console.log(response1);
    //
    // const response = await this.table14Repository
    //   .createQueryBuilder('abcd')
    //   .delete()
    //   .where(`name LIKE '%insert%'`)
    //   // .getQuery();
    //   .execute();
    // console.log(response);
    // //
    // const data = [];
    // for (let i = 0; i < 5; i++) {
    //   const tags = [];
    //   const randomTagLength = 0 + Math.floor(Math.random() * 4);
    //   for (let j = 0; j < randomTagLength; j++) {
    //     const tag = new Tag();
    //     tag.name = `insert-name${i + 1}-${j + 1}`;
    //     tag.description = `insert-description${i + 1}-${j + 1}`;
    //     tags.push(tag);
    //   }
    //   const table14 = new Table14();
    //   table14.name = `insert-name${i + 1}`;
    //   table14.num = 1 + Math.floor(Math.random() * 10);
    //   table14.tags = tags;
    //   data.push(table14);
    // }
    // const response2 = await this.table14Repository.save(data);
    // console.log(response2);
    //
    // const response = await this.connection
    //   .createQueryBuilder()
    //   .select('tb14.name', 'table14Name')
    //   .addSelect('tags.name', 'tagName')
    //   // .addSelect('tags.description', 'tagDescription')
    //   .from(Table14, 'tb14')
    //   .leftJoin('tb14.tags', 'tags')
    //   .getRawMany();
    // console.log(
    //   response.map(item => [
    //     item.table14Name,
    //     item.tagName,
    //     // item.tagDescription,
    //   ]),
    // );
    //
    // const response = await this.table14Repository
    //   .createQueryBuilder()
    //   .select('SUM(num)', 'name')
    //   .groupBy('id')
    //   // .getMany();
    //   .getRawMany();
    // console.log(response);
    //
    // const response = await this.table14Repository
    //   .createQueryBuilder('abcd')
    //   .getQuery();
    // console.log(response);
    //
    // const response1 = await this.table14Repository
    //   .createQueryBuilder('abcd')
    //   .select('abcd.name', 'pablo')
    //   .getQuery();
    // // .getRawMany();
    // console.log(response1);
    //
    // const response = await this.table14Repository
    //   .createQueryBuilder('abcd')
    //   .where('abcd.name = :name', { name: 'insert-name3' })
    //   // don't use the same parameter key, because it will replace the value
    //   // of the earlier one.
    //   // check the formed query and its parameters to see the issue.
    //   .orWhere('abcd.num = :name', { name: 3 })
    //   .getQueryAndParameters();
    // // .getMany();
    // console.log(response);
    //
    // const response = await this.table14Repository
    //   .createQueryBuilder('abcd')
    //   .where('abcd.name IN (:...names)', {
    //     names: ['insert-name1', 'insert-name3', 'insert-name4'],
    //   })
    //   // .getQueryAndParameters();
    //   .getMany();
    // console.log(response);
    //
    // const response = await this.table14Repository
    //   .createQueryBuilder()
    //   .where('name LIKE %insert%')
    //   .orWhere('num > :num', { num: 2 })
    //   .orWhere(
    //     new Brackets(qb => {
    //       qb.where('num > :num1', { num1: 3 });
    //       qb.andWhere('num > :num2', { num2: 4 });
    //       qb.orWhere(
    //         new Brackets(qb => {
    //           qb.where('num > :num3', { num3: 5 });
    //           qb.andWhere('num > :num4', { num4: 6 });
    //         }),
    //       );
    //     }),
    //   )
    //   .getQueryAndParameters();
    // console.log(response);
    //
    // you can also join non-related entities and tables
    // https://typeorm.io/#/select-query-builder/joining-any-entity-or-table
    //
    // const response = await this.table14Repository
    //   .createQueryBuilder('abcd')
    //   .leftJoinAndSelect('abcd.tags', 'tags', 'tags.name = :name', {
    //     name: 'insert-name1-2',
    //   })
    //   .getMany();
    // console.log(response);
    //
    // const response = await this.table14Repository
    //   .createQueryBuilder('abcd')
    //   .leftJoinAndMapMany('abcd.tagsMap', 'abcd.tags', 'tags')
    //   // // MapOne will map only the first one found.
    //   //.leftJoinAndMapOne('abcd.tagsMap', 'abcd.tags', 'tags')
    //   .getMany();
    // console.log(response);
    //
    // const response = await this.table14Repository
    //   .createQueryBuilder('abcd')
    //   // doesn't print?
    //   // check out https://github.com/nestjs/nest/issues/685#issuecomment-464311624
    //   .printSql()
    //   .getMany();
    // console.log(response);
    //
    // const response = await this.table14Repository
    //   .createQueryBuilder('abcd')
    //   .where(qb => {
    //     const subQuery = qb
    //       .subQuery()
    //       .select('sub1.name')
    //       .from(Table14, 'sub1')
    //       .where('1 == :value', { value: 1 })
    //       .getQuery();
    //     return `abcd.name IN ${subQuery}`;
    //   })
    //   .getQueryAndParameters();
    // console.log(response);
    //
    // // Note: you can literally have a raw sql query in any of the
    // // queryBuilder functions.
    // // look at the above example, the where function's parameter will
    // // result in the string at the end.
    // // try seperating that where part in another query builder and then put
    // // that query (get a query using .getQuery()) in the where clause, for eg.
    // // where(`abcd.name IN ${qb1.getQuery()}`)
    // // more at https://typeorm.io/#/select-query-builder/using-subqueries
    //
    // const response = await this.table14Repository
    //   .createQueryBuilder('abcd')
    //   // you can use .subQuery() to add brackets around any query.
    //   // subQuery() can be used on any of these three:
    //   // (connection|entityManager|repository).createQueryBuilder()
    //   .subQuery()
    //   .select()
    //   .from(Tag, 'abcd1')
    //   .getQuery();
    // console.log(response);
    //
    // // using connection
    // const response = await this.connection
    //   .createQueryBuilder()
    //   .select('abcd2.name')
    //   // .select('abcd2.abcd1_name')
    //   .from(subQuery => {
    //     return (
    //       subQuery
    //         .select('abcd1.name', 'name')
    //         // .select('abcd1.name')
    //         .from(Table14, 'abcd1')
    //         .where('abcd1.name IS NOT NULL')
    //     );
    //   }, 'abcd2')
    //   // .getQuery();
    //   .getRawMany();
    // console.log(response);
    //
    // const response = await this.table14Repository
    //   .createQueryBuilder('abcd')
    //   .select('abcd.name')
    //   .from(subQuery => {
    //     return subQuery
    //       .select('abcd1.name')
    //       .from(Table14, 'abcd1')
    //       .where('abcd1.name IS NOT NULL');
    //   }, 'abcd2')
    //   // .getQuery();
    //   .getRawMany();
    // console.log(response);
    //
    // // this is same as the above example.
    // const response = await this.connection
    //   .createQueryBuilder()
    //   .select('abcd2.name')
    //   .from(subQuery => {
    //     return subQuery
    //       .select('abcd1.name', 'name')
    //       .from(Table14, 'abcd1')
    //       .where('abcd1.name IS NOT NULL');
    //   }, 'abcd2')
    //   .from(Table14, 'abcd')
    //   // .getQuery();
    //   .getRawMany();
    // console.log(response);
    //
    // const response = await this.connection
    //   .createQueryBuilder()
    //   .select('abcd.name')
    //   .addSelect(subQuery => {
    //     return (
    //       subQuery
    //         .select('abcd1.name', 'name')
    //         .from(Table14, 'abcd1')
    //         // if subquery is in select statement, then it shouldn't return
    //         // more than one row.
    //         // .where('abcd1.name IS NOT NULL');
    //         .where('abcd1.name = :name', { name: 'insert-name1' })
    //     );
    //   }, 'abcd2')
    //   .from(Table14, 'abcd')
    //   // .getQuery();
    //   .getRawMany();
    // console.log(response);
    //
    // const response = await this.table14Repository
    //   .createQueryBuilder('abcd')
    //   .select(['abcd.name', 'abcd.num'])
    //   .getMany();
    // // // execute() returns raw data.
    // // .execute();
    // console.log(response);
    //
    // const data = [];
    // for (let i = 0; i < 5; i++) {
    //   const aa = new Table14();
    //   aa.name = `delete-this${i + 1}`;
    //   data.push(aa);
    // }
    // const response = await this.table14Repository
    //   .createQueryBuilder()
    //   .insert()
    //   .values(data)
    //   // .execute();
    //   .getQuery();
    // console.log(response);
    //
    // const response = await this.table14Repository
    //   .createQueryBuilder()
    //   .delete()
    //   .where("name LIKE '%delete%'")
    //   .execute();
    // // .getQuery();
    // console.log(response);
    //
    // raw sql support in insert:
    // https://typeorm.io/#/insert-query-builder/raw-sql-support
    // eg.
    // .values({
    //   firstName: "Timber",
    //   lastName: () => "CONCAT('S', 'A', 'W')"
    // })
    //
    const table14 = await this.table14Repository
      .createQueryBuilder('abcd')
      .select('abcd')
      .leftJoinAndSelect('abcd.tags', 'tags')
      .getMany();
    // console.log(table14);
    // console.log(table14.map(item => item.tags));
  }
}
