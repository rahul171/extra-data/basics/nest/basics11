import { IsBoolean, IsOptional, IsString } from 'class-validator';

export class SaveUserDto {
  @IsString()
  name: string;

  @IsString()
  message: string;

  @IsBoolean()
  @IsOptional()
  isActive: boolean;
}
