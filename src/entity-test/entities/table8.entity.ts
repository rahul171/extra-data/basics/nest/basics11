import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Table8 {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;
}
