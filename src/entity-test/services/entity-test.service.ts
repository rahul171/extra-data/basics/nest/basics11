import { Injectable } from '@nestjs/common';
import { InjectEntityManager, InjectRepository } from '@nestjs/typeorm';
import { Table2 } from '../entities/table2.entity';
import { EntityManager, Equal, Repository } from 'typeorm';
import { Table1 } from '../entities/table1.entity';
import { Table3 } from '../entities/table3.entity';
import { Table4 } from '../entities/table4.entity';
import { Table5 } from '../entities/table5.entity';
import { Table6 } from '../entities/table6.entity';
import { Table8 } from '../entities/table8.entity';
import { Table7 } from '../entities/table7.entity';
import { Table9 } from '../entities/table9.entity';
import { Table10 } from '../entities/table10.entity';
import { Roles, Table11 } from '../entities/table11.entity';

@Injectable()
export class EntityTestService {
  constructor(
    @InjectRepository(Table2)
    private table2Repository: Repository<Table2>,
    @InjectEntityManager()
    private entityManager: EntityManager,
  ) {
    // this.main();
  }

  async main() {
    // const table2 = new Table2();
    // table2.title = 'hello';
    // // table2.createdAt = new Date(Date.now() + 24 * 60 * 60);
    // table2.createdAt = new Date();
    //
    // const response = await this.table2Repository.save(table2);
    // console.log(response);
    //
    // const response = await this.table2Repository.find();
    // response.forEach(item => {
    //   console.log('from db =>', item.createdAt);
    //   console.log(item.createdAt.constructor.toString());
    //   console.log('current date =>', new Date());
    // });
    // const table2 = new Table2();
    // table2.title = 'title1';
    //
    // const table2 = await this.table2Repository.findOne(1, {
    //   relations: ['table12'],
    // });
    //
    // const table1 = new Table1();
    // table1.name = 'name1';
    //
    // // table2.table12 = [table1];
    // table2.table12.push(table1);
    // const response = await this.table2Repository.save(table2);
    // console.log(response);
    //
    // const table2 = await this.table2Repository.findOne(1, {
    //   relations: ['table12'],
    // });
    // console.log(table2.table12);
    //
    // table2.table12 = table2.table12.filter(item => item.id !== 7);
    // console.log(table2);
    //
    // const response = await this.table2Repository.save(table2);
    // console.log(response);
    //
    // const itemToSoftRemove = table2.table12.find(item => item.id === 8);
    //
    // // not working
    // const response = await this.table2Repository.softRemove(itemToSoftRemove);
    // console.log(response);
    //
    // const response = await this.entityManager
    //   .getRepository(Table2)
    //   .createQueryBuilder('tab')
    //   .leftJoinAndSelect('tab.table12', 'table12FromQueryBuilder')
    //   .getMany();
    // console.log(response);
    //
    // const table3 = new Table3();
    // table3.name3 = 'name3-1';
    //
    // const table1 = new Table1();
    // table1.name = 'name1';
    // table1.table3 = table3;
    //
    // const table1 = await this.entityManager
    //   .getRepository(Table1)
    //   .createQueryBuilder('table1')
    //   .getOne();
    // console.log(table1);
    //
    // const table2 = new Table2();
    // table2.title = 'title1';
    // table2.table12 = [table1];
    //
    // const table2 = await this.table2Repository.findOne(2, {
    //   relations: ['table12'],
    // });
    // console.log(table2);
    //
    // table2.table12.push(table1);
    //
    // const response = await this.table2Repository.save(table2);
    // console.log(response);
    // // can't load table3, as its 2 level deep, and relations array only look
    // // for 1 level deep, relations for table2 only.
    // // use queryBuilder to solve this.
    // const table2 = await this.table2Repository.findOne(1, {
    //   relations: ['table12'],
    // });
    //
    // const response = await this.entityManager
    //   .getRepository(Table2)
    //   .createQueryBuilder('table2')
    //   .leftJoinAndSelect('table2.table12', 'table1')
    //   .leftJoinAndSelect('table1.table3', 'table3')
    //   .getMany();
    //
    // console.log('table2 =>', response);
    // console.log(
    //   'table2.table12 =>',
    //   response.map(item => item.table12),
    // );
    // console.log(
    //   'table2.table12.table3 =>',
    //   response.map(item => item.table12.map(item => item.table3)),
    // );
    //
    // const table3 = new Table3();
    // table3.name3 = 'name31';
    //
    // const response = await this.entityManager
    //   .getRepository(Table3)
    //   .save(table3);
    // console.log(response);
    //
    // const table3 = await this.entityManager.getRepository(Table3).findOne(11);
    // table3.name3 = 'updated1-name31';
    //
    // const response = await this.entityManager
    //   .getRepository(Table3)
    //   .save(table3);
    // console.log(response);
    //
    // const table4 = new Table4();
    // table4.name1 = 'name1';
    //
    // const response = await this.entityManager
    //   .getRepository(Table4)
    //   .save(table4);
    // console.log(response);
    //
    // const response = await this.entityManager.getRepository(Table4).find();
    // console.log(response);
    //
    // const table5 = new Table5();
    // table5.name = 'name1';
    // table5.message = 'message1';
    //
    // const response = await this.entityManager
    //   .getRepository(Table5)
    //   .save(table5);
    // console.log(response);
    //
    // const table5 = await this.entityManager
    //   .getRepository(Table5)
    //   // findOne() tables primary key as first parameter.
    //   .findOne('name1');
    // console.log(table5);
    //
    // const parent = await this.entityManager.getRepository(Table6).findOne(10);
    //
    // const table6 = new Table6();
    // table6.name = 'name1';
    // table6.parent = parent;
    //
    // const response = await this.entityManager
    //   .getRepository(Table6)
    //   .save(table6);
    // console.log(response);
    //
    // const response = await this.entityManager
    //   .getRepository(Table6)
    //   // will get only 1 level.
    //   .find({ relations: ['parent'] });
    // console.log(response);
    //
    // if you use the queryBuilder, then also you have to manually get
    // each level (i.e. write joinAndSelect() function for every level).
    //
    // maybe @Tree() can work. have to try.
    //
    // const parent = await this.entityManager
    //   .getRepository(Table6)
    //   .find({ relations: ['children'] });
    // console.log('parent =>', parent);
    // console.log(
    //   'parent.children =>',
    //   parent.map(item => item.children),
    // );
    // console.log(
    //   'parent.children.children =>',
    //   parent.map(item => item.children),
    // );
    //
    // the method of leftJoinAndSelect() is also limited, you have to choose how
    // much deeper you want to go, it can't automatically go the deepest.
    // not efficient overall, because you shouldn't have to write
    // leftJoinAndSelect() for each and every level you want to get.
    // const parent = await this.entityManager
    //   .getRepository(Table6)
    //   .createQueryBuilder('table6')
    //   .leftJoinAndSelect('table6.children', 'table6Children1')
    //   .leftJoinAndSelect('table6Children1.children', 'table6Children2')
    //   .leftJoinAndSelect('table6Children2.children', 'table6Children3')
    //   .leftJoinAndSelect('table6Children3.children', 'table6Children4')
    //   .leftJoinAndSelect('table6Children4.children', 'table6Children5')
    //   .leftJoinAndSelect('table6Children5.children', 'table6Children6')
    //   .leftJoinAndSelect('table6Children6.children', 'table6Children7')
    //   .getMany();
    // // console.log('parent =>', parent);
    // this.recursivePrintTable6(parent);
    //
    // console.log('');
    // console.log('');
    //
    // const parent1 = await this.entityManager
    //   .getRepository(Table6)
    //   .createQueryBuilder('table6')
    //   // see the query to understand how it works.
    //   // its like writing join statements one by one.
    //   .leftJoinAndSelect('table6.children', 'table6Children1')
    //   .innerJoinAndSelect('table6Children1.children', 'table6Children2')
    //   .leftJoinAndSelect('table6Children2.children', 'table6Children3')
    //   .leftJoinAndSelect('table6Children3.children', 'table6Children4')
    //   .leftJoinAndSelect('table6Children4.children', 'table6Children5')
    //   .leftJoinAndSelect('table6Children5.children', 'table6Children6')
    //   .leftJoinAndSelect('table6Children6.children', 'table6Children7')
    //   .getMany();
    // // .getQuery();
    // // console.log(parent1);
    // this.recursivePrintTable6(parent1);
    //
    // const table8 = new Table8();
    // table8.name = 'name1';
    //
    // const table7 = new Table7();
    // table7.name = 'name1';
    // table7.table8 = table8;
    //
    // const response = await this.entityManager
    //   .getRepository(Table7)
    //   .save(table7);
    // console.log(response);
    //
    // const table7 = await this.entityManager.getRepository(Table7).findOne(1);
    // console.log(table7);
    //
    // const parent = await this.entityManager.getRepository(Table9).findOne(9);
    //
    // const table9 = new Table9();
    // table9.name = 'name10';
    // table9.parent = parent;
    //
    // const response = await this.entityManager
    //   .getRepository(Table9)
    //   .save(table9);
    // console.log(response);
    //
    // const response = await this.entityManager
    //   .getRepository(Table9)
    //   .find({ relations: ['children'] });
    // console.log(response);
    //
    // const table10 = new Table10();
    // table10.id = 1;
    // table10.name = 'name1';
    //
    // const response = await this.entityManager
    //   .getRepository(Table10)
    //   .save(table10);
    // console.log(response);
    //
    // const table11 = new Table11();
    // table11.name = 'name2';
    // table11.content = Roles.EDITOR;
    //
    // const response = await this.entityManager
    //   .getRepository(Table11)
    //   .save(table11);
    // console.log(response);
    //
    // const response = await this.entityManager.getRepository(Table11).find();
    // console.log(response);
    //
    // const table11 = new Table11();
    // table11.name = 'name4';
    // table11.content1 = [Roles.ADMIN, Roles.EDITOR];
    //
    // const response = await this.entityManager
    //   .getRepository(Table11)
    //   .save(table11);
    // console.log(response);
    //
    // const response = await this.entityManager.getRepository(Table11).find();
    // console.log(response);
    //
    // const table11 = new Table11();
    // table11.name = 'name4';
    // table11.content1 = [Roles.ADMIN, Roles.EDITOR];
    //
    // const response = await this.entityManager
    //   .getRepository(Table11)
    //   .save(table11);
    // console.log(response);
    //
    // won't let you save if you have , in your enum value,
    // also if you try to make the change directly in the db,
    // for eg. changing "editor" to "edi,tor", then it will just delete the
    // "editor" value from that column of that row.
    // pretty nice.
    //
    // const table11 = new Table11();
    // table11.name = 'name1';
    // table11.contentJson = {
    //   name: 'jsonName',
    //   a: 'b',
    //   c: { d: 'e', f: { g: 1, h: 'i' } },
    //   j: [1, 2, 3, 4, 55, 66],
    // };
    //
    // const response = await this.entityManager
    //   .getRepository(Table11)
    //   .save(table11);
    // console.log(response);
    //
    // const response = await this.entityManager.getRepository(Table11).find({
    //   where: {
    //     name: Equal('name2'),
    //   },
    // });
    // console.log(response);
    //
  }

  recursivePrintTable6(item, parentIndex = 0) {
    if (!item || (Array.isArray(item) && item.length === 0)) {
      return;
    }
    item.forEach(item => {
      console.log(
        `${this.getPad(parentIndex)}`,
        {
          id: item.id,
          name: item.name,
        },
        !item.children || item.children.length === 0 ? 'END' : '',
      );
      this.recursivePrintTable6(item.children, parentIndex + 1);
    });
  }

  getPad(n = 1, char = '-') {
    let s = '';
    for (let i = 0; i < n * 4; i++) {
      s += char;
    }
    return s;
  }
}
