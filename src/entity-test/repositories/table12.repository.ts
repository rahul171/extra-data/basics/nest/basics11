import { EntityManager, EntityRepository, Repository } from 'typeorm';
import { Table12 } from '../entities/table12.entity';
import { Table13 } from '../entities/table13.entity';

@EntityRepository(Table12)
export class Table12Repository extends Repository<Table12> {
  constructor(private entityManager: EntityManager, ...args) {
    super();

    // console.log('more args', args);
    // console.log('repository', this.entityManager.getRepository(Table12));

    // entity manager also works here.
    this.entityManager
      .getRepository(Table13)
      .find()
      .then(data => {
        console.log('table12Repository', data);
      });
  }

  async getName() {
    return this.findOne();
  }
}
