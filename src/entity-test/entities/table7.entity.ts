import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Table8 } from './table8.entity';

@Entity()
export class Table7 {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  // get id of table 8 without specifying relations in find() or
  // joining in queryBuilder.
  @Column()
  table8Id: number;

  @OneToOne(() => Table8, {
    cascade: true,
  })
  @JoinColumn()
  table8: Table8;
}
