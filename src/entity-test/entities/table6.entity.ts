import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Table6 {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @ManyToOne(() => Table6)
  parent: Table6;

  @OneToMany(
    () => Table6,
    table6 => table6.parent,
  )
  children: Table6[];
}
