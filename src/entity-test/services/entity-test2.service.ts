import { Injectable } from '@nestjs/common';
import { InjectEntityManager } from '@nestjs/typeorm';
import { Between, EntityManager, Like, MoreThan, Raw } from 'typeorm';
import { Table12Repository } from '../repositories/table12.repository';
import { Table12 } from '../entities/table12.entity';
import { Table13 } from '../entities/table13.entity';
import { Table12AbstractRepository } from '../repositories/table12-abstract.repository';
import { Table12WithoutExtendRepository } from '../repositories/table12-without-extend.repository';

@Injectable()
export class EntityTest2Service {
  private table12Repository: Table12Repository;
  private table12AbstractRepository: Table12AbstractRepository;
  private table12WithoutExtendRepository: Table12WithoutExtendRepository;

  constructor(
    @InjectEntityManager()
    private entityManager: EntityManager,
  ) {
    // this.table12Repository = this.entityManager.getCustomRepository(
    //   Table12Repository,
    // );
    // this.table12AbstractRepository = this.entityManager.getCustomRepository(
    //   Table12AbstractRepository,
    // );
    // this.table12WithoutExtendRepository = this.entityManager.getCustomRepository(
    //   Table12WithoutExtendRepository,
    // );
    // this.runThis();
  }

  async runThis() {
    // const table12 = new Table12();
    // table12.name = 'name7';
    //
    // const response = await this.table12Repository.save(table12);
    // console.log(response);
    //
    // const response = await this.table12Repository.getName();
    // console.log(response);
    //
    // let response = await this.table12Repository.find({});
    // response = response.map(item => {
    //   item.num = 1 + Math.floor(Math.random() * 10);
    //   return item;
    // });
    // const res = await this.table12Repository.save(response);
    // console.log(res);
    //
    // const res = await this.table12Repository.find();
    // console.log(res);
    //
    // const response = await this.table12Repository.findAndCount({
    //   where: {
    //     // num: Raw('id + 3'),
    //     // num: Raw(val => `${val} IN (1,2,3,4,5)`),
    //     num: Between(1, 6),
    //   },
    // });
    // console.log(response);
    //
    // const res = await this.table12Repository.find();
    // console.log(res);
    //
    // delete res[5].id;
    //
    // res.forEach(item => {
    //   console.log(this.table12Repository.hasId(item));
    //   console.log(this.table12Repository.getId(item));
    // });
    //
    // const table12 = new Table12();
    // const updatedTable12 = this.table12Repository.merge(
    //   table12,
    //   { name: 'name1' },
    //   { num: 10 },
    // );
    // const response = await this.table12Repository.save(updatedTable12);
    // console.log(response);
    //
    // const res = await this.table12Repository.find();
    // console.log(res);
    //
    // const obj = {
    //   id: 11,
    //   name: 'preload-name1',
    //   num: 11,
    // };
    //
    // const response = await this.table12Repository.preload(obj);
    // console.log(response);
    //
    // const res = await this.table12Repository.find();
    // console.log(res);
    //
    // const response = await this.table12Repository.find({
    //   // OR
    //   // where: [{ num: MoreThan(9) }, { name: Like('%1%') }],
    //   // AND
    //   // where: { num: MoreThan(9), name: Like('%1%') },
    //   //
    //   where: { num: MoreThan(9) },
    // });
    // console.log(response);
    //
    // const removeResponse = await this.table12Repository.remove(response);
    // console.log(removeResponse);
    //
    // const res = await this.table12Repository.find();
    // console.log(res);
    //
    // const table13 = new Table13();
    // table13.name = 'name2';
    //
    // const response = await this.entityManager
    //   .getRepository(Table13)
    //   .save(table13);
    // console.log(response);
    //
    // const table12 = this.table12Repository.create({
    //   name: 'name1',
    //   num: 12,
    //   table13,
    // });
    //
    // const response1 = await this.table12Repository.save(table12);
    // console.log(response1);
    //
    // const res = await this.table12Repository.find();
    // console.log(res);
    //
    // const table12 = await this.table12Repository.findOne({
    //   id: 17,
    // });
    // console.log(table12);
    //
    // // onDelete = 'CASCADE' isn't working here.
    // const removeResponse = await this.table12Repository.delete(table12);
    // console.log(removeResponse);
    //
    // const res = await this.table12Repository.find();
    // console.log(res);
    //
    // const response = await this.table12Repository.findOne();
    // console.log(response);
    //
    // takes entity as params.
    // const removeResponse = await this.table12Repository.remove();
    //
    // takes criteria as params.
    // const deleteResponse = await this.table12Repository.delete({
    //   name: 'name1',
    // });
    // console.log(deleteResponse);
    //
    // const res = await this.table12Repository.find();
    // console.log(res);
    //
    // const response = await this.table12Repository.increment(
    //   { name: 'name2' },
    //   'num',
    //   2,
    // );
    // console.log(response);
    //
    // const res1 = await this.table12Repository.find();
    // console.log(res1);
    //
    // const res = await this.table12Repository.find();
    // console.log(res);
    //
    // const table13 = await this.entityManager.findOne(Table13, 3);
    // console.log(table13);
    //
    // const response = await this.table12Repository.insert({
    //   name: 'insert-name1',
    //   table13,
    // });
    // console.log(response);
    //
    // const res1 = await this.table12Repository.find();
    // console.log(res1);
    //
    // const res = await this.table12Repository.find();
    // console.log(res);
    //
    // just set the deleted date column.
    // const response = await this.table12Repository.softDelete({
    //   id: 18,
    // });
    // console.log(response);
    //
    // const response1 = await this.table12Repository.restore(18);
    // console.log(response1);
    //
    // const response = await this.table12AbstractRepository.getAll();
    // console.log(response);
    //
    // const response1 = await this.table12AbstractRepository.save({
    //   name: 'name1',
    // });
    // console.log(response1);
    //
    // const response = await this.table12WithoutExtendRepository.getAllTable12();
    // console.log(response);
    //
    // const response1 = await this.table12WithoutExtendRepository.getAllTable13();
    // console.log(response1);
    //
  }
}
