import {
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Table16 } from './table16.entity';

@Entity()
export class Table15 {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @OneToOne(
    () => Table16,
    table16 => table16.table151,
  )
  @JoinColumn()
  table161: Table16;

  @OneToMany(
    () => Table16,
    table15 => table15.table152,
  )
  table162: Table16;

  @ManyToMany(
    () => Table16,
    table16 => table16.table153,
  )
  @JoinTable()
  table163: Table16;
}
