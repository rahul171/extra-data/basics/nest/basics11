import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { Table15 } from '../entities/table15.entity';
import { Connection, Repository } from 'typeorm';

@Injectable()
export class EntityTest4Service {
  constructor(
    @InjectRepository(Table15)
    private table15Repository: Repository<Table15>,
    @InjectConnection()
    private connection: Connection,
  ) {
    // using setTimeout to push it to the end of execution stack.
    // since libuv will wait for the current execution to finish.
    // not doing so => runs the function when nest is initializing,
    // so if there are any print statement in the function,
    // it will get printed in between the nest initialization messages.
    setTimeout(() => {
      this.runThis();
    }, 0);
  }

  async runThis() {
    console.log('hello');
  }
}
