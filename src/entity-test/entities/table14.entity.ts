import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Tag } from './tags.entity';

@Entity()
export class Table14 {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({
    default: 0,
  })
  num: number;

  @OneToMany(
    type => Tag,
    table15 => table15.table14,
    {
      cascade: true,
    },
  )
  tags: Tag[];
}
