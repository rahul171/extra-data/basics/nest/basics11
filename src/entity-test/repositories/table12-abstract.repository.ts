import { AbstractRepository, EntityManager, EntityRepository } from 'typeorm';
import { Table12 } from '../entities/table12.entity';
import { Table13 } from '../entities/table13.entity';

@EntityRepository(Table12)
export class Table12AbstractRepository extends AbstractRepository<Table12> {
  constructor(private entityManager: EntityManager, ...args) {
    super();

    console.log('more args', args);
    console.log('repository', this.entityManager.getRepository(Table12));

    // entity manager also works here.
    this.entityManager
      .getRepository(Table13)
      .find()
      .then(data => {
        console.log('table13FromAbstract', data);
      });
  }

  async save(table12: Table12): Promise<Table12> {
    return this.repository.save(table12);
  }

  async getAll(): Promise<Table12[]> {
    return this.repository.find();
  }
}
