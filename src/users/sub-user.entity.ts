import {
  Column,
  Entity,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { User } from './user.entity';

@Entity()
export class SubUser {
  @PrimaryGeneratedColumn()
  id: number;

  @PrimaryColumn()
  // @Column()
  childName: string;

  @Column()
  childMessage: string;

  // @OneToMany(
  //   () => User,
  //   user => user.subUser,
  // )
  // user: User;
}
