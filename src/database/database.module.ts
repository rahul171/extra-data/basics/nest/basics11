import { Module } from '@nestjs/common';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { Table1 } from '../entity-test/entities/table1.entity';
import { Table2 } from '../entity-test/entities/table2.entity';
import { Table3 } from '../entity-test/entities/table3.entity';
import { Table4 } from '../entity-test/entities/table4.entity';
import { Table5 } from '../entity-test/entities/table5.entity';
import { Table6 } from '../entity-test/entities/table6.entity';
import { Table7 } from '../entity-test/entities/table7.entity';
import { Table8 } from '../entity-test/entities/table8.entity';
import { Table9 } from '../entity-test/entities/table9.entity';
import { Table10 } from '../entity-test/entities/table10.entity';
import { Table11 } from '../entity-test/entities/table11.entity';
import { Table12 } from '../entity-test/entities/table12.entity';
import { Table13 } from '../entity-test/entities/table13.entity';
import { Table14 } from '../entity-test/entities/table14.entity';
import { Tag } from '../entity-test/entities/tags.entity';
import { Table15 } from '../entity-test/entities/table15.entity';
import { Table16 } from '../entity-test/entities/table16.entity';

const databaseConfig: TypeOrmModuleOptions = {
  type: 'mysql',
  host: 'localhost',
  port: 3306,
  username: 'root',
  password: 'abcd1234',
  database: 'nest-typeorm-test',
  // entities: ['dist/**/*.entity{.ts,.js}'],
  entities: [
    Table1,
    Table2,
    Table3,
    Table4,
    Table5,
    Table6,
    Table7,
    Table8,
    Table9,
    Table10,
    Table11,
    Table12,
    Table13,
    Table14,
    Tag,
    Table15,
    Table16,
  ],
  synchronize: true,
};

@Module({
  imports: [TypeOrmModule.forRoot(databaseConfig)],
  exports: [TypeOrmModule],
})
export class DatabaseModule {}
