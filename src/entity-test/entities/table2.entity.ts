import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Table1 } from './table1.entity';

@Entity()
export class Table2 {
  @PrimaryGeneratedColumn()
  id: number;

  // @PrimaryGeneratedColumn('uuid')
  // id2: string;

  @Column()
  title: string;

  @ManyToOne(() => Table1)
  table11: Table1;

  @CreateDateColumn()
  createdAt: Date;

  @ManyToMany(() => Table1, {
    cascade: true,
  })
  @JoinTable()
  table12: Table1[];
}
