import { EntityManager, EntityRepository, Repository } from 'typeorm';
import { Table12 } from '../entities/table12.entity';
import { Table13 } from '../entities/table13.entity';

@EntityRepository()
export class Table12WithoutExtendRepository {
  private table12Repository: Repository<Table12>;
  private table13Repository: Repository<Table13>;

  constructor(private entityManager: EntityManager, ...args) {
    this.table12Repository = this.entityManager.getRepository(Table12);
    this.table13Repository = this.entityManager.getRepository(Table13);

    // no more args here. but its there in the other 2 repository classes/files.
    console.log('more args', args);
    console.log('repository', this.entityManager.getRepository(Table12));
  }

  async getAllTable12(): Promise<Table12[]> {
    return this.table12Repository.find();
  }

  async getAllTable13(): Promise<Table13[]> {
    return this.table13Repository.find();
  }
}
