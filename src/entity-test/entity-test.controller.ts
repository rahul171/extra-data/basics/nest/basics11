import { Controller, Get } from '@nestjs/common';
import { EntityTestService } from './services/entity-test.service';

@Controller('entity-test')
export class EntityTestController {
  constructor(private entityTestService: EntityTestService) {}

  @Get()
  home() {
    return 'welcome to the entity test';
  }
}
