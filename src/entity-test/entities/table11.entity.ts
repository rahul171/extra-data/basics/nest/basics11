import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

export enum Roles {
  ADMIN = 'admin',
  EDITOR = 'editor',
  GHOST = 'ghost',
}

@Entity()
export class Table11 {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    comment: 'hey there',
  })
  name: string;

  @Column({
    type: 'enum',
    enum: Roles,
    default: Roles.ADMIN,
    comment: 'this is enum type',
  })
  content: Roles;

  @Column({
    type: 'set',
    enum: Roles,
    default: [Roles.EDITOR, Roles.GHOST],
  })
  content1: Roles[];

  @Column({
    type: 'simple-json',
  })
  contentJson: any;
}
